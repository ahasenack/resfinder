Source: resfinder
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>, Nilesh Patra <nilesh@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/med-team/resfinder
Vcs-Git: https://salsa.debian.org/med-team/resfinder.git
Homepage: https://bitbucket.org/genomicepidemiology/resfinder
Rules-Requires-Root: no

Package: resfinder
Architecture: all
Depends: ${python3:Depends},
         ${misc:Depends},
         resfinder-db,
         python3-cgecore,
         python3-tabulate,
         ncbi-blast+-legacy,
         kma,
         python3-git,
         python3-dateutil
Description: identify acquired antimicrobial resistance genes
 ResFinder identifies acquired antimicrobial resistance genes in total or
 partial sequenced isolates of bacteria.
 .
 ResFinder that uses BLAST for identification of acquired antimicrobial
 resistance genes in whole-genome data. As input, the method can use both
 pre-assembled, complete or partial genomes, and short sequence reads
 from four different sequencing platforms. The method was evaluated on
 1862 GenBank files containing 1411 different resistance genes, as well
 as on 23 de-novo-sequenced isolates.

Package: resfinder-example
Architecture: all
Depends: ${misc:Depends},
Recommends: resfinder
Multi-Arch: foreign
Description: identify acquired antimicrobial resistance genes (example data)
 ResFinder identifies acquired antimicrobial resistance genes in total or
 partial sequenced isolates of bacteria.
 .
 ResFinder that uses BLAST for identification of acquired antimicrobial
 resistance genes in whole-genome data. As input, the method can use both
 pre-assembled, complete or partial genomes, and short sequence reads
 from four different sequencing platforms. The method was evaluated on
 1862 GenBank files containing 1411 different resistance genes, as well
 as on 23 de-novo-sequenced isolates.
 .
 This package contains example data to test resfinder.
